#from DatumBox import DatumBox
import DatumBox
import tweepy
import re
import webapp2
from lib import oauth2 as oauth
import urllib2 as urllib
import logging
from google.appengine.ext import ndb
from google.appengine.api import taskqueue

API_KEY = "67eb401216a033ff5de37ddd37f5ed55"
access_token_key ="75440688-Z8da4n0Zm5MvaLRo304b30eaP5lLSbWMILAUlNa5H" 
access_token_secret ="To0331mKVZBj02RXXPJDNq7L86QAx7WHNf4OlVHtE" 

consumer_key = "8jiZd3pYoQK2VLONAtYgJg"
consumer_secret = "CqsWgTyvtX3TTyR0lPvqxJcDEwRoxsW6wMNKCuI2s"

_debug = 0

oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_method = "POST"

http_handler  = urllib.HTTPHandler(debuglevel=_debug)
https_handler = urllib.HTTPSHandler(debuglevel=_debug)

class URLParams(ndb.Model):
    since_id = ndb.IntegerProperty()
    q = ndb.StringProperty(required = True)
    lang = ndb.StringProperty()
    include_entities = ndb.StringProperty()
    tqueue = ndb.StringProperty()

    def as_dict(self):
        d = {"since_id":self.since_id,"q":self.q,"lang":self.lang,"include_entities":self.include_entities}
        return d

class SentimentDb(ndb.Model):
    sentiment = ndb.JsonProperty()
    q = ndb.StringProperty(required = True)

q_RE = re.compile(r"^[a-zA-Z0-9_\-\@\#\!\:\)\(]{1,20}$")
def valid_q(q):
        return q and q_RE.match(q)

def AddQparams(url,searchObj):
    first = True 
    for k in searchObj.keys():
        s = searchObj[k]
        if s.__class__ == str:
            s  = urllib.quote(s)
        if first:
            url = url + k + "=" + s
            first = False
        else:
            print s
            url = url + "&" + k + "=" + s
    return url       

#Need to find out if query parameter to api.search can be passed as list. 
#Need to find a way to access allowed_params :  
#search = bind_api(path = '/search/tweets.json',payload_type = 'search_results',
#allowed_param = ['q', 'lang', 'locale', 'since_id', 'geocode', 'show_user', 'max_id', 'since', 'until', 'result_type']
#)
def twitterSearchReq(queryParams):
    lang_qp = queryParams['lang']
    since_id_qp = queryParams['since_id']
    q = queryParams['q']
    include_entities_qp = queryParams['since_id']

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token_key, access_token_secret)
    api = tweepy.API(auth,search_host='api.twitter.com')
    result_list = api.search(q, since_id=since_id_qp, lang=lang_qp, include_entities=include_entities_qp)
    
    return result_list
    

class QueryStore(webapp2.RequestHandler):
    def get(self):
        logging.info('In QueryStore Request')
        q = self.request.get('q')
        tqueue = self.request.get('tqueue')
        cron_url  = URLParams(id=q)
        cron_url.populate(since_id=0,q=q, lang='en', include_entities='false',tqueue=tqueue) 
        ckey = cron_url.put()
        senti = SentimentDb(id=q)
        senti.populate(sentiment={'positive':0, 'negative':0, 'neutral':0 },q=q)
        skey = senti.put()
        self.response.write('Added ckey,skey to the database : %s : %s' % (ckey,skey))

class CronReq(webapp2.RequestHandler):
    def get(self):
        logging.info('In Cron Request')
        q = self.request.get('q')
        qgstore = URLParams.get_by_id(id=q)
        self.response.headers['Content-Type'] = 'text/plain'
        if qgstore:
            logging.info('before calling the  twitter api : %s' % qgstore.since_id)
            parameters = {}
            logging.info('qgstore : %s' % qgstore)
            result_list = twitterSearchReq(qgstore.as_dict())
            qgstore.since_id = result_list.max_id
            qgstore.put()
            logging.info('after calling the  twitter api : %s' % qgstore.since_id)
            queue = taskqueue.Queue(qgstore.tqueue)
            logging.info('Putting tasks in the queue : %s' % queue)
            # task =[]
            for i in result_list:
                tasks = []
                logging.info('tweet : %s' % i.text)
                payload_str = i.text
                tasks.append(taskqueue.Task(payload=payload_str, method='PULL'))
                queue.add(tasks)
                # ideally the get method should now redirect the request to other handler
                # that handles processing of the task queue
                        
            datum_box = DatumBox.DatumBox(API_KEY) 
            sentiRec = SentimentDb.get_by_id(id=q)
            
            logging.info('Leasing tasks from the queue : %s' % queue)
            leasetasks = queue.lease_tasks(300,15)
            for lt in leasetasks:
               datum_sentiment  = datum_box.twitter_sentiment_analysis(lt.payload ).__str__() 
               sentiRec.sentiment[datum_sentiment] = sentiRec.sentiment[datum_sentiment] + 1
               logging.info("twitter sentiment :  %s"  %  datum_sentiment)
            queue.delete_tasks(leasetasks)
            logging.info(" Before put sentiment negative : positive: neutral |  %s : %s :%s  | %s" % (sentiRec.sentiment['negative'],sentiRec.sentiment['positive'],sentiRec.sentiment['neutral'],sentiRec.q))
            
            sentiRec.put()    
           
            #entity = SentimentDb(q=q, sentiment= sentiRec.sentiment)
            #sentiRec.populate(sentiment=sentiRec.sentiment)    
            #entity.put()
           
            logging.info(" After put : sentiment negative : positive: neutral |  %s : %s :%s  | %s" % (sentiRec.sentiment['negative'],sentiRec.sentiment['positive'],sentiRec.sentiment['neutral'],sentiRec.q))
            return
        else:
            logging.error("Search query %s not set up in database " % q)
       
application = webapp2.WSGIApplication([('/task/CronReq', CronReq),('/task/querystore', QueryStore)], debug = True)
