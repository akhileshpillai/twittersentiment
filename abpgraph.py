import webapp2
import logging
import gviz_api
import twitter15 

# Put the url (http://google-visualization.appspot.com/python/dynamic_example) as your
# Google Visualization data source.

class DataSourceSentiment(webapp2.RequestHandler):
    def get(self):
        logging.debug("In sentiment datasource GET call")
        req_id=0
        tqx = self.request.get('tqx', None)
        if tqx:
            req_id = dict([p.split(':') for p in self.request.get('tqx', '').split(';')]).get('reqId', 0)
            
        logging.info("tqx: %s" % tqx)
        logging.info("req_id: %s" % req_id)
        description = {('Sentiment',"string"):("Count","number")}  
        data_table = gviz_api.DataTable(description)
        q = self.request.get('q')
        dataDS = twitter15.SentimentDb.get_by_id(id=q)
        data = dataDS.sentiment 
        logging.info(" data: %s " % data) 
        logging.info(" data_class: %s " % data.__class__) 
        data_table.LoadData(data)
        logging.info(data_table.ToJSonResponse(req_id=req_id))
        
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write(data_table.ToJSonResponse(req_id=req_id))

class MainPage(webapp2.RequestHandler):
    def get(self):
        logging.debug("In the GET call")
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, World!')

application = webapp2.WSGIApplication([('/', MainPage), ('/ds/sentiment', DataSourceSentiment)], debug = True)
